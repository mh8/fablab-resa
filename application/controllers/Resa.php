<?php
class Resa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('resa_model');
        $this->load->helper('url');

        $prefs = array(
        'start_day'    => 'monday',
        'month_type'   => 'long',
        'day_type'     => 'long',
        'show_next_prev' => TRUE,
        'next_prev_url'   => 'http://localhost/fablab-resa/index.php/resa/index/',
        'show_other_days' => TRUE
        );


        //This part controls the html structure of the calendar
        $prefs['template'] = '

        {table_open}<div class="table-responsive rounded"><table class="table table-bordered">{/table_open}

        {heading_row_start}<thead><tr>{/heading_row_start}

        {heading_previous_cell}<th><a href="{previous_url}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" ><div class="mois">{heading}</div></th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></th>{/heading_next_cell}

        {heading_row_end}</tr></thead>{/heading_row_end}

        {week_row_start}<tbody><tr>{/week_row_start}
        {week_day_cell}<td width="14.28%"><div class="day_week">{week_day}</div></td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_start_today}<td class="day_on">{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month"><div class="dayday">{/cal_cell_start_other}

        {cal_cell_content}<div class="dayday">{day}<div class="rezzza">{content}</div></div>{/cal_cell_content}
        {cal_cell_content_today}<div class="highlight">{day}<div class="rezzza">{content}</div></div>{/cal_cell_content_today}

        {cal_cell_no_content}<div class="dayday">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}<div class="dayday">{day}</div>{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</div></td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</tbody></table></div>{/table_close}
        ';

        $this->load->library('calendar', $prefs);
    }

    public function index()
    {
        //This will check if the month is set in the URI, if not, it will set it using date()
        $month = (!$this->uri->segment(4)) ? date('m') : $this->uri->segment(4);
        $year = (!$this->uri->segment(3)) ? date('Y') : $this->uri->segment(3);

        //Load all the data
        $data['title'] = 'Calendrier des réservations';
        $data['list_resa'] = 'Liste des réservations';
        $data['make_resa'] = 'Faire une réservation';
        $data['cal_resa'] = $this->resa_model->get_list_resa($month, $year);
        $data_cal = $this->resa_model->get_month_resa($month, $year);
        $data['cal'] = $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4), $data_cal);

        //Load all the views
        $this->load->view('templates/header', $data);
        $this->load->view('resa/index', $data);
        $this->load->view('resa/calendar', $data);
        $this->load->view('resa/list', $data, $month);
        $this->load->view('resa/create', $data);
        $this->load->view('templates/footer');
    }

    //This part needs to be worked on.
    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $month = (!$this->uri->segment(4)) ? date('m') : $this->uri->segment(4);
        $year = (!$this->uri->segment(3)) ? date('Y') : $this->uri->segment(3);

        //Load all the data
        $data['title'] = 'Calendrier des réservations';
        $data['list_resa'] = 'Liste des réservations';
        $data['make_resa'] = 'Faire une réservation';
        $data['cal_resa'] = $this->resa_model->get_list_resa($month, $year);
        $data_cal = $this->resa_model->get_month_resa($month, $year);
        $data['cal'] = $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4), $data_cal);

        $this->form_validation->set_rules('person', 'Nom', 'required');
        $this->form_validation->set_rules('machine', 'Machine', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('time_start', 'Heure début', 'required');
        $this->form_validation->set_rules('time_end', 'Heure fin', 'required');

        if ($this->form_validation->run() === false) {
            //Load all the views
            $this->load->view('templates/header', $data);
            $this->load->view('resa/index', $data);
            $this->load->view('resa/calendar', $data);
            $this->load->view('resa/list', $data);
            $this->load->view('resa/create', $data);
            $this->load->view('templates/footer');
        } else {
            $this->resa_model->set_resa();
            $data['cal_resa'] = $this->resa_model->get_list_resa($month, $year);
            $this->load->view('templates/header', $data);
            $this->load->view('resa/success');
            $this->load->view('templates/footer');
        }
    }
}
