<!-- faire une reservation -->
<div class="row">
<div class="col-lg-12">
<h2 id="formulaire"><?php echo $make_resa; ?></h2>

<?php
$this->load->helper('form');
$this->load->library('form_validation');

echo validation_errors(); ?>

<?php echo form_open('resa/create'); ?>

<div class="form-group">
<label for="person">Nom <span class="obligatoire">(obligatoire)</span></label>
<input type="input" class="form-control" name="person" required="required"/><br />
</div>


<div class="form-group">
<label for="machine">Machine <span class="obligatoire">(obligatoire)</span></label>
<select class="form-control" name="machine" required>
  <option name="" value="">Votre machine ?</option>
  <option name="machine" value="Laser">Laser</option>
  <option name="machine" value="CNC">CNC</option>
  <option name="machine" value="Ultimaker">Ultimaker</option>
  <option name="machine" value="RISO">RISO</option>
</select>
</div>


<hr />
<label for="date">Choisir votre jour <span class="obligatoire">(obligatoire)</span></label>
<div id="sandbox-container">

	<div class="input-group date">
  <input type="text" class="form-control" name="date" required="required"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
</div>
</div>

<hr />

<div class="col-lg-6 debut" class="form-group">
<label for="time_start">Heure début <span class="obligatoire">(obligatoire)</span></label>
<select class="form-control" name="time_start" required>
  <option name="" value="">Votre horaire de début ?</option>
  <option name="time_start" value="10">10</option>
  <option name="time_start" value="11">11</option>
  <option name="time_start" value="12">12</option>
  <option name="time_start" value="13">13</option>
  <option name="time_start" value="14">14</option>
  <option name="time_start" value="15">15</option>
  <option name="time_start" value="16">16</option>
  <option name="time_start" value="17">17</option>
  <option name="time_start" value="18">18</option>
  <option name="time_start" value="19">19</option>
  <option name="time_start" value="20">20</option>
</select>
</div>

<div class="col-lg-6 fin"  class="form-group">
<label for="time_end">Heure fin <span class="obligatoire">(obligatoire)</span></label>
<select class="form-control" name="time_end" required>
  <option name="" value="">Votre horaire de fin ?</option>
  <option name="time_start" value="11">11</option>
  <option name="time_start" value="12">12</option>
  <option name="time_start" value="13">13</option>
  <option name="time_start" value="14">14</option>
  <option name="time_start" value="15">15</option>
  <option name="time_start" value="16">16</option>
  <option name="time_start" value="17">17</option>
  <option name="time_start" value="18">18</option>
  <option name="time_start" value="19">19</option>
  <option name="time_start" value="20">20</option>
  <option name="time_start" value="21">21</option>
</select>
</div><br />

<button type="submit" name="submit" class="btn btn-default">Valider réservation</button>

</form>

<script type="text/javascript">
$('#sandbox-container .input-group.date').datepicker({
  maxViewMode: 0,
    todayBtn: "linked",
    language: "fr",
    daysOfWeekHighlighted: "0",
    toggleActive: true
});
</script>
</div>
</div>
<!-- fin de faire une reservation -->
