<!-- liste des reservations -->
<div class="row">
  <div class="col-lg-12">

<div class="liste-adherents">
<h2><?php echo $list_resa; ?></h2>
<ul>
<?php foreach ($cal_resa as $resa_item): ?>

        <h3><?php echo $resa_item['date']; ?></h3>
        <li class="liste-resa">
                <!--<span class="info hide "><?php echo $resa_item['id']; ?></span>-->
                <span class="info"><?php echo $resa_item['person']; ?> / </span>
                <span class="info_default <?php echo $resa_item['machine']; ?>"><?php echo $resa_item['machine']; ?></span>
                <span class="info"> / de <?php echo $resa_item['time_start']; ?> h</span>
                <span class="info">à <?php echo $resa_item['time_end']; ?> h</span>
                <!--<span class="info"><?php echo $resa_item['slug']; ?></span>-->
        </li>

<?php endforeach; ?>
</ul>
</div>

</div>
</div>
<!-- fin de liste des reservations -->
