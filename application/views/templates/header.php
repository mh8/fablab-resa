<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Pop [lab] Réservation</title>

<link rel="stylesheet" type="text/css" href="/fablab-resa/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/fablab-resa/css/bootstrap-datepicker.css" />
<link rel="stylesheet" type="text/css" href="/fablab-resa/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" type="text/css" href="/fablab-resa/css/resa.css" />

 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/fablab-resa/js/bootstrap.min.js"></script>
<script src="/fablab-resa/js/bootstrap-datepicker.min.js"></script>
<script src="/fablab-resa/js/bootstrap-datepicker.fr.min.js"></script>
</head>

<body>

<nav class="navbar navbar-default navbar-collapse" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Le Calendrier du Pop [lab]</a>
    </div>
  </div>
</nav>

<!-- Entete -->
<div class="container">
<header>
<h1><?php echo $title; ?></h1>
</header>
</div>
<!-- Fin Entete -->

<!-- principal -->
<div class="container">
<section>
