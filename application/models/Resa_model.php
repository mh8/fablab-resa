<?php
class Resa_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_list_resa($month, $year)
    {
        /*if ($slug === false) {
            $query = $this->db->get('cal_resa');
            return $query->result_array();
        }*/
        $sql = "SELECT * FROM cal_resa WHERE month = '$month' AND year = '$year'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //This method fetches data from the db and parses it into an array for the calendar
    public function get_month_resa($month, $year)
    {
        $sql = "SELECT * FROM cal_resa WHERE month = '$month' AND year = '$year'";
        $query = $this->db->query($sql);

        /*
        The expected array format for the calendar is :
        $data_cal = array(
          day => 'content',
          e.g.
          20 => 'test'
        );
        */

        $data_cal = array();

        foreach ($query->result_array() as $key => $value) {
          if (array_key_exists($value['day'], $data_cal)){
            $data_cal[$value['day']] = $data_cal[$value['day']].'<div class="'.$value['machine'].'">'.$value['time_start']." - ".$value['time_end']." : ".$value['person']." - ".$value['machine'].'</div>';

          }
          else {
            $data_cal[$value['day']] = '<div class="'.$value['machine'].'">'.$value['time_start']." - ".$value['time_end']." : ".$value['person']." - ".$value['machine'].'</div>';
          }
        }

        /*
        The $query->result_array() returns an array with a form of :
        array (
          0 => Array,
          ...
        )
        Each sub array being the equivalent of a $query->row() :
        array(
          id => 1,
          person => Fabman,
          ...
        )
        */

        return $data_cal;
    }

    public function set_resa()
    {
        $this->load->helper('url');

        //This slug generation is probably useless, it appears to do nothing in the db.
        $slug = url_title($this->input->post('id'), 'dash', true);

        //Here we parse the date to automatically fill in the day/month/year columns of db
        $date = $this->input->post('date');
        $split = str_split(str_replace("/", "", $date), 2);
        $split = array_values($split);
        $day = $split[0];
        $month = $split [1];
        $year = $split[2].$split[3];

        //Create the array we will inject in the db
        $data = array(
        'person' => $this->input->post('person'),
        'machine' => $this->input->post('machine'),
        'date' => $date,
        'day' => $day,
        'month' => $month,
        'year' => $year,
        'time_start' => $this->input->post('time_start'),
        'time_end' => $this->input->post('time_end')
    );

        return $this->db->insert('cal_resa', $data);
    }
}
