-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 26, 2018 at 05:04 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cal_poplab`
--

-- --------------------------------------------------------

--
-- Table structure for table `cal_resa`
--

CREATE TABLE `cal_resa` (
  `id` int(11) NOT NULL,
  `person` varchar(128) NOT NULL,
  `machine` varchar(128) NOT NULL,
  `date` tinytext NOT NULL,
  `day` int(2) NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `time_start` int(2) NOT NULL,
  `time_end` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cal_resa`
--

INSERT INTO `cal_resa` (`id`, `person`, `machine`, `date`, `day`, `month`, `year`, `time_start`, `time_end`) VALUES
(1, 'Fabman', 'Laser', '16/10/2018', 16, 10, 2018, 14, 16),
(2, 'John', 'CNC', '17/11/2018', 17, 11, 2018, 14, 16),
(3, 'Fabman', 'CNC', '16/10/2018', 16, 10, 2018, 10, 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cal_resa`
--
ALTER TABLE `cal_resa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cal_resa`
--
ALTER TABLE `cal_resa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
