# Fablab Reservation

A simple online calendar tool for fablab users to book the use of a machine.

It uses the CodeIgniter php Framework for the back-end and bootstrap for css.


## To Do :

[void]

### In master branch :

- include a clean sample .sql for import (careful, indicate the date, and a link to the proper month)
- Change database name from cal_poplab to cal_fablab in master
- Finish a clean documentation of the project and install instructions

### In Poplab branch :

- Add content to nav (links to poplab wiki & maisonpop website)
- DEPLOY : Edit config file to proper database info


## Improvements to look into :

- Add a logic to the hour start / hour end so that a selected hour end cannot be before a selected hour start
- Block reservations on sundays


## Installation instructions (unfinished!):

### Basic setup :

- Using phpMyAdmin, build a database named cal_fablab
- Import the sample table named 'cal_resa.sql' in that database
- Create a user and grant him SELECT/INSERT rights on the cal_resa table in the cal_fablab databases
- Open the file /config/database.php and modify lines 79 & 80 with the user logins you created (default user/pass is : calendar/cal2018dar)

### Tweaking the views and functions

- If you're familiar with php/MySQL you can tweak the way the controller works in the following file : /application/controllers/Resa.php
- If you're familiar with HTML you can tweak the way display's structure in the following files : /application/views/resa and /application/views/templates (header/footer)
- If you're familiar with CSS you can tweak the css in : /css and the javascript in /js


## Useful ressources :

https://dzone.com/articles/codeigniter-getting-started-with-the-calendar-clas

To access the user guide of Code Igniter go to http://yourdomain.wtv/user_guide/
